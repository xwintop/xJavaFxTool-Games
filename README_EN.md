**xJavaFxTool exchange QQ group：== [387473650](https://jq.qq.com/?_wv=1027&k=59UDEAD) ==**

#### Project Description:
XJavaFxTool - Games was developed by using the deployment headaches and practical small tool set [xJavaFxTool] (https://gitee.com/xwintop/xJavaFxTool) a collection of plug-ins, which can realize the framework automatically loaded after packaging, available online to download and update tool, subsequent small tool to add in this project, to implement dynamic load the jar package.

#### Environmental construction instructions:
- The development environment is jdk1.8, based on maven build
- Developed with eclipase or Intellij Idea (Recommended to use [Intellij Idea](https://www.jetbrains.com/idea/) )
- This project uses [lombok](https://projectlombok.org/) . If you have not downloaded the lombok plugin when viewing this project, please install it first, otherwise you can't find the get/set method.
- The dependent [xcore](https://gitee.com/xwintop/xcore) package has been uploaded to the git-hosted maven platform. The git hosting maven can refer to the tutorial (if you can't download it, please pull the project to compile it yourself ). Tutorial address: Click to enter

#### The currently integrated gadgets are:
1. 2048: Small game 2048
2. BullsAndCowsGame: A number guessing game
3. Sudoku:Sudoku Game
