**xJavaFxTool交流QQ群：== [387473650](https://jq.qq.com/?_wv=1027&k=59UDEAD) ==**

#### 项目简介：
xJavaFxTool-Games是使用javaFx开发的实用小工具集[xJavaFxTool](https://gitee.com/xwintop/xJavaFxTool)的插件集合，可实现打包后让框架自动加载，可在线下载和更新工具，后续小工具将在这个项目中添加，实现动态jar包加载。

#### 环境搭建说明：
- 开发环境为jdk1.8，基于maven构建
- 使用eclipase或Intellij Idea开发(推荐使用[Intellij Idea](https://www.jetbrains.com/idea/))
- 本项目使用了[lombok](https://projectlombok.org/),在查看本项目时如果您没有下载lombok 插件，请先安装,不然找不到get/set等方法
- 依赖的[xcore包](https://gitee.com/xwintop/xcore)已上传至git托管的maven平台，git托管maven可参考教程(若无法下载请拉取项目自行编译)。[教程地址：点击进入](http://blog.csdn.net/u011747754/article/details/78574026)

#### 插件开发说明：
- 根据相应模版开发javafx项目，在文件src/main/resources/config/toolFxmlLoaderConfiguration.xml中定义插件相应配置，具体说明如下：
配置示例：
```
<root>
	<ToolFxmlLoaderConfiguration title="littleTools" menuId="p-littleTools" menuParentId="moreToolsMenu" isMenu="true" />
	<ToolFxmlLoaderConfiguration>
		<url>/com/xwintop/xJavaFxTool/fxmlView/littleTools/CharacterConverter.fxml</url>
		<resourceBundleName>locale.CharacterConverter</resourceBundleName>
		<className></className>
		<title>Title</title>
		<isDefaultShow></isDefaultShow>
		<menuId></menuId>
		<menuParentId>p-littleTools</menuParentId>
		<controllerType>Node</controllerType>
	</ToolFxmlLoaderConfiguration>
</root>
```
配置简单说明：
url;//资源url
resourceBundleName;//国际化资源文件
className;//class名称
title;//标题（配合国际化资源文件，无着默认显示原字符）
iconPath;//图标路径
isDefaultShow;// 是否默认打开
menuId;// 菜单id
menuParentId;// 菜单父id
isMenu;//是否为菜单（true为菜单）
controllerType = "Node";// 内容类型

#### 目前集成的小工具有：
1. 2048：小游戏2048
2. BullsAndCowsGame:猜数字小游戏
3. Sudoku:数独游戏